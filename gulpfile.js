const gulp = require('gulp'),
      sass = require('gulp-sass'),
      browserSync = require('browser-sync')
;

const {watch, task} = require('gulp')

gulp.task('sass', function () {
    return gulp.src('app/sass/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function(){
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    })
});
task('dev', () => {
    browserSync.init({
        server: {
            baseDir: './app'
        }
    });
    watch('app/sass/**/*.scss').on('change', browserSync.reload);
    watch('app/js/**/*.js').on('change', browserSync.reload);
    watch('app/index.html').on('change', browserSync.reload);
});
// gulp.task('watch', ['browser-sync', 'sass'], function (){
// //     gulp.watch('app/sass/**/*.scss', ['sass']);
// //     gulp.watch('app/*.html', browserSync.reload);
// //     gulp.watch('app/js/**/*.js', browserSync.reload);
// // });


